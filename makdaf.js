#!/usr/bin/env node

const program = require("commander"),
  ini = require("ini"),
  fs = require("fs"),
  path = require("path"),
  chalk = require('chalk');

const version = "0.1.0";

const error = chalk.white.bgRed.bold;
const ok = chalk.white.bgGreen.bold

program
  .version(version)
  .description('A tool for generating .desktop files')
  .option("-e, --exec <file>", "The executable of the application, possibly with arguments", null)
  .option("-n, --name [name]", "The application name, or maybe an abbreviation/acronym", null)
  .option("-g, --generic-name [generic-name]", "what you would generally call an application that does what this specific application offers (i.e. Firefox is a \"Web Browser \")", null)
  .option("-i, --icon [icon]", "The name of the icon that will be used to display this entry", null)
  .option("-c, --comment [comment]", "Any usefull additional information", null)
  .parse(process.argv);

if (!program.exec) {
  console.error(error("ERROR:"), "Path to executable (-e) is required");
  process.exit(1);
}

if (!fs.existsSync(program.exec)) {
  console.error(error("ERROR:"), "Given executable does not exist : " + path.normalize(program.exec));
  process.exit(1);
}

let appName = path.basename(program.exec);
if (program.name) appName = program.name;

let userHome = process.env.HOME || process.env.HOMEPATH || process.env.USERPROFILE;
let desktopFileName = path.join(userHome, '/.local/share/applications/', appName + '.desktop');
fs.closeSync(fs.openSync(desktopFileName, "w"));

let config = ini.parse(fs.readFileSync(desktopFileName, "utf-8"));

config.Version = "1.0";
config.Type = "Application";
config.Terminal = "false";
config.Exec = path.normalize(program.exec);
config.Name = appName;

if (program.icon) {
  if (!fs.existsSync(program.icon)) {
    console.error(error("ERROR:"), "Given icon file does not exist : " + program.icon);
    process.exit(1);
  }
  config.Icon = program.icon;
}

if (program.genericName) {
  config.GenericName = program.genericName;
}

if (program.comment) {
  config.Comment = program.comment;
}

console.log('Creating launcher file : ' + path.normalize(desktopFileName));
fs.writeFileSync(desktopFileName, ini.stringify(config, {
  section: "Desktop Entry"
}));